package dev.lpf.mpdemo.order.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lpf
 * @since 2019-09-07
 */
@Controller
@RequestMapping("/order")
public class OrderController {

}

