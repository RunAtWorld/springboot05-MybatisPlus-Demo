package dev.lpf.mpdemo.order.mapper;

import dev.lpf.mpdemo.order.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lpf
 * @since 2019-09-07
 */
public interface OrderMapper extends BaseMapper<Order> {

}
