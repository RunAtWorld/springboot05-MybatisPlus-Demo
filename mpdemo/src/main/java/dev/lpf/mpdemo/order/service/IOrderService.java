package dev.lpf.mpdemo.order.service;

import dev.lpf.mpdemo.order.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpf
 * @since 2019-09-07
 */
public interface IOrderService extends IService<Order> {

}
