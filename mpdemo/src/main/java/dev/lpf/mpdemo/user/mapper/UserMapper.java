package dev.lpf.mpdemo.user.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import dev.lpf.mpdemo.user.entity.User;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lpf
 * @since 2019-09-06
 */
public interface UserMapper extends BaseMapper<User> {

}
