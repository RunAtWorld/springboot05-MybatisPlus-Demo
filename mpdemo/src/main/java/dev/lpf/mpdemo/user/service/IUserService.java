package dev.lpf.mpdemo.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import dev.lpf.mpdemo.user.entity.User;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lpf
 * @since 2019-09-06
 */
public interface IUserService extends IService<User> {

}
