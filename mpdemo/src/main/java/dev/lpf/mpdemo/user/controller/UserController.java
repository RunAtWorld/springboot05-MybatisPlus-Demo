package dev.lpf.mpdemo.user.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lpf
 * @since 2019-09-06
 */
@Controller
@RequestMapping("/user")
public class UserController {

}

